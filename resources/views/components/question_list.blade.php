<div id="accordion">
    @if(!empty($categories))
    @php $i = 1; @endphp
    @foreach($categories as $category)
    <div class="card mb-2">
        <div class="card-header" id="heading{{$category->id}}">
            <h5 class="mb-0">
                @if($i == 1)
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$category->id}}"
                    aria-expanded="true" aria-controls="collapse{{$category->id}}">
                    {{$category->name}}
                </button>
                @else
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$category->id}}"
                    aria-expanded="false" aria-controls="collapse{{$category->id}}">
                    {{$category->name}}
                </button>
                @endif
            </h5>
        </div>

        @if($i == 1)
        <div id="collapse{{$category->id}}" class="collapse show" aria-labelledby="heading{{$category->id}}"
            data-parent="#accordion">
        </div>
        @else
        <div id="collapse{{$category->id}}" class="collapse" aria-labelledby="heading{{$category->id}}"
            data-parent="#accordion">
        </div>
        @endif
        <div class="card-body" style="height: 300px; overflow-y: scroll;">
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Question</th>
                            <th scope="col">Options</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questions as $question)
                        @if($question->category_id == $category->id)
                        <tr>
                            <th scope="row">1</th>
                            <td>{{$question->question}}</td>
                            <td>
                                <ol type="A">
                                    @foreach($question->options as $option)
                                    <li>
                                        {{$option}}
                                    </li>
                                    @endforeach
                                </ol>
                            </td>
                            <td>
                                <button onclick="editQuestion(this, '{{$question->question_id}}')">
                                    Edit
                                </button>
                                <button onclick="deleteQuestion(this, '{{$question->question_id}}')">
                                    Delete
                                </button>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@php $i++; @endphp
@endforeach
@else
<div class="alert alert-warning">
    No Questions Yet...
</div>
@endif
</div>