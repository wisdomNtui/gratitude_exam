<?php

namespace App\Http\Controllers;

use App\Category;
use App\Option;
use App\Questions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExamController extends Controller
{
    /**
     * Show Home page
     * @return object Laravel View Instance
     */
    public function index(Request $request)
    {
        // Fetch Categories For Category Select Field
        $categories = Category::get();

        return view('home', compact('categories'));
    }

    /**
     * Display Exam Categories
     * @return String JSON Response
     */
    public function get_category(Request $request, $category_id = null)
    {
        try {
            if (!empty($category_id)) {
                $category = Category::where('id', $category_id)->first();

                return view('components.category_edit', compact('category'));
            } else {
                $categories = Category::get();

                return view('components.category_list', compact('categories'));
            }
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Populate Category Input
     * @return String JSON Response
     */
    public function category_select(Request $request)
    {
        try {
            $categories = Category::get();

            return view('components.category_select', compact('categories'));
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Add Exam Category
     * @return String JSON Response
     */
    public function add_category(Request $request)
    {
        try {
            $category = new Category();
            $category->name = $request->name;
            $category->save();

            return response()->json(['success' => true, 'message' => "Category Created Successfully"], 201);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Update Exam Category
     * @return String JSON Response
     */
    public function update_category(Request $request)
    {
        try {
            $category = Category::find($request->category_id);
            $category->name = $request->name;
            $category->save();

            return response()->json(['success' => true, 'message' => "Category Updated Successfully"], 201);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Delete Exam Category
     * @return String JSON Response
     */
    public function delete_category(Request $request, $category_id)
    {
        try {
            $category = Category::find($category_id);
            $category->delete();

            return response()->json(['success' => true, 'message' => "Category Deleted Successfully"], 200);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Add Exam Question
     * @return String JSON Response
     */
    public function add_question(Request $request)
    {
        try {
            $data = [
                'question' => $request->name,
            ];

            $question = new Questions($data);
            $category = Category::find($request->category);
            $last_inserted = $category->question()->save($question);
            foreach ($request->options as $opt) {
                $option = new Option();
                $option->name = $opt;
                $option->question_id = $last_inserted->id;
                $option->save();
            }

            return response()->json(['success' => true, 'message' => "Question Created Successfully"], 201);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Display Questions
     * @return String JSON Response
     */
    public function get_question(Request $request, $question_id = null)
    {
        try {
            if (!empty($question_id)) {
                $question = Questions::join('options', 'options.question_id', '=', 'questions.id')
                    ->select(['questions.question AS question', 'questions.id AS question_id', 'questions.category_id AS category_id', DB::raw('group_concat(options.name) as options'), DB::raw('group_concat(options.id) as options_id')])->where('questions.id', $question_id)->groupBy('questions.id')->first();

                // Fetch Categories
                $categories = Category::get();

                // Convert Question Options To Array
                $question->options = explode(',', $question->options);

                return view('components.question_edit', compact('question', 'categories'));
                //return response()->json(['success' => true, 'message' => $question->options], 200);
            } else {
                $questions = Questions::join('options', 'options.question_id', '=', 'questions.id')
                    ->select(['questions.question AS question', 'questions.id AS question_id', 'questions.category_id AS category_id', DB::raw('group_concat(options.name) as options')])->groupBy('questions.id')->get();

                $categories = Category::get();

                // Convert Question Options To Array
                foreach ($questions as $question) {
                    $question->options = explode(',', $question->options);
                }

                return view('components.question_list', compact('categories', 'questions'));
                //return response()->json(['success' => true, 'message' => compact('questions', 'categories')], 200);
            }
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Update Question
     * @return String JSON Response
     */
    public function update_question(Request $request)
    {
        try {
            $question = Questions::find($request->question_id);
            $question->question = $request->name;
            $question->category_id = $request->category;
            $question->save();
            $option_arr = explode(',', $request->option_id);
            $i = 0;
            foreach ($option_arr as $opt) {
                $option = Option::find($opt);
                $option->name = $request->options[$i];
                $option->save();
                //$test_arr[$i] = $opt;
                $i++;
            }

            return response()->json(['success' => true, 'message' => "Question Updated Successfully"], 200);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * Delete Question
     * @return String JSON Response
     */
    public function delete_question(Request $request, $question_id)
    {
        try {
            $question = Questions::find($question_id);
            $question->delete();

            return response()->json(['success' => true, 'message' => "Question Deleted Successfully"], 200);
        } catch (\Throwable $th) {
            Log::error($th);
            return response()->json(['success' => false, 'message' => $th->getMessage()], 500);
        }
    }

}
