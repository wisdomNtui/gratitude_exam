<ul class="list-group">
    @if(!empty($categories))
        @foreach($categories as $category)
            <li class="list-group-item">
                <span>
                {{$category->name}}
                </span>
                <span class="float-right">
                    <button onclick="editCategory(this, '{{$category->id}}')">Edit</button>
                    <button onclick="deleteCategory(this, '{{$category->id}}')">Delete</button>
                </span>
            </li>
        @endforeach
    @else
        <div class="alert alert-warning">
            There are no categories yet
        </div>
    @endif
</ul>
