<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function option()
    {
        return $this->hasMany('App\Option');
    }
}
