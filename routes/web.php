<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

// Homepage Route
Route::get('/', 'ExamController@index');

// Route To Add Question Category
Route::post('add-category', 'ExamController@add_category');

// Route To Get Question Category List
Route::get('get-category/{category_id?}', 'ExamController@get_category');

// Route To Update Question Category
Route::post('update-category', 'ExamController@update_category');

// Route To Delete Question Category
Route::get('delete-category/{category_id}', 'ExamController@delete_category');

// Route To Get Question Category
Route::get('get-category/{category_id?}', 'ExamController@get_category');

// Route To Add Question
Route::post('add-question', 'ExamController@add_question');

// Route To Get Question List
Route::get('get-question/{question_id?}', 'ExamController@get_question');

// Route To Update Question
Route::post('update-question', 'ExamController@update_question');

// Route To Delete Question
Route::get('delete-question/{question_id}', 'ExamController@delete_question');

// Route To Populate the Category Select Input
Route::get('category-select', 'ExamController@category_select');
