<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>
	<title>wisdomntui</title>

	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Personal View Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- css files -->
	<link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="{{ url('assets/css/style.css') }}" type="text/css" media="all" /> <!-- Style-CSS -->
	<link rel="stylesheet" href="{{ url('assets/css/fontawesome-all.css') }}"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->

</head>

<body>


	<!-- about -->
	<section class="">
		<div class="container py-3">
			<div class="row border">
				<div class="col-md-6">
					<h5 class="p-2 bg-dark mb-2 text-white">Please Add A Question Category</h5>
					<div class="colmd-6">
						<div class="alert alert-success d-none" id="category-alert" role="alert">

						</div>
						<form method="post" id="add-category">
							@csrf
							<div>
								<label for="category">Category</label>
								<input type="text" name="name" required>
							</div>
							<div>
								<button type="submit">Add</button>
							</div>
						</form>
					</div>
					<hr>
					<h5 class="text-center">Category List</h5>
					<div class="border p-3" id="category-view" style="height: 500px; overflow-y: scroll;">

					</div>
				</div>
				<div class="col-md-6">
					<h5 class="p-2 bg-dark mb-2 text-white">Please Add A Question</h5>
					<div class="">
						<div class="alert alert-success d-none" id="question-alert" role="alert">

						</div>
						<form method="post" id="add-question">
							@csrf
							<div>
								<label for="category">Question:</label>
								<textarea name="name" style="width: 100%;" rows="10" required></textarea>
							</div>
							<div class="mb-3">
								<h6 for="category"><strong>Options:</strong></h6>
								<div class="mb-1">
									<label for="category">A</label>
									<input type="text" name="options[]" required>
								</div>
								<div class="mb-1">
									<label for="category">B</label>
									<input type="text" name="options[]" required>
								</div>
								<div class="mb-1">
									<label for="category">C</label>
									<input type="text" name="options[]" required>
								</div>
								<div>
									<label for="category">D</label>
									<input type="text" name="options[]" required>
								</div>

							</div>
							<div class="mb-3">
								<label for="category">Category:</label>
								<div id="category-select">

								</div>
							</div>
							<div>
								<button type="submit">Add Question</button>
							</div>
						</form>
					</div>
					<hr>
					<h5 class="text-center">Questions</h5>
					<div class="border" id="question-view" style="height: 500px; overflow-y: scroll;">

					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Hold Edit Modals -->
	<section>
		<div id="cat-edit-view">

		</div>
		<div id="quest-edit-view">

		</div>
	</section>
	<!-- Hold Edit Modals -->





	<!-- js -->
	<script type="text/javascript" src="{{ url('assets/js/jquery-2.1.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/bootstrap.js') }}"></script>
	<script>
		// Render all display components on page load
		$(document).ready(function () {
			callOnMod();
		});
		// Render all display components on page load



		/******************** Category Section Starts *********************/
		// Add question categories
		$('#add-category').submit(el => {
			el.preventDefault()
			addCategory(el)
			callOnMod()
		});

		function addCategory(el) {
			let url = `{{ url('add-category') }}`;
			let data = new FormData(el.target)

			goPost(url, data)
				.then(res => {
					if (res.status === 200 || res.status === 201) {
						$("#category-alert").html(res.message);
						$("#category-alert").removeClass('d-none');
						fadeAlert();
					}
				})
				.catch(err => {
					console.error(err)
				})
		}
		// Add question categories

		// View question categories
		function displayCategory() {
			let getUrl = "{{url('get-category')}}";

			$.get(getUrl, function (data, status) {
				$("#category-view").empty();
				$("#category-view").html(data);
			});
		}
		// View question categories

		// Edit Question Categories
		function editCategory(el, categoryId) { // Function to Populate and Pop Edit Modal (GET)
			let getUrl = "{{url('get-category')}}";
			getUrl = getUrl + "/" + categoryId;

			$.get(getUrl, function (data, status) {
				$("#cat-edit-view").empty();
				$("#cat-edit-view").html(data);
				$("#category-modal").modal("show");
			});
		}

		function updateCategory(el) { // Function to update category (POST)
			let url = `{{ url('update-category') }}`;
			let data = new FormData(el.target)

			goPost(url, data)
				.then(res => {
					if (res.status === 200 || res.status === 201) {
						$("#category-alert").html(res.message);
						$("#category-alert").removeClass('d-none');
						fadeAlert();
					}
				})
				.catch(err => {
					console.error(err)
				})
		}
		// Edit Question Categories

		// Delete Category
		function deleteCategory(el, categoryId) { // Function to Populate and Pop Edit Modal (GET)
			let getUrl = "{{url('delete-category')}}";
			getUrl = getUrl + "/" + categoryId;

			$.get(getUrl, function (data, status) {
				callOnMod();
				$("#category-alert").html(data.message);
				$("#category-alert").removeClass('d-none');
				fadeAlert();
			});
		}
		// Delete Category

		// Populate Category Dropdown
		function categorySelect() {
			let getUrl = "{{url('category-select')}}";

			$.get(getUrl, function (data, status) {
				$("#category-select").empty();
				$("#category-select").html(data);
			});
		}
		// Populate Category Dropdown

		/****************  Category Section Ends **********************/

		// View question 
		function displayQuestions() {
			let getUrl = "{{url('get-question')}}";

			$.get(getUrl, function (data, status) {
				$("#question-view").empty();
				$("#question-view").html(data);
				console.log(data)
			});
		}
		// View question 

		// Add questions
		$('#add-question').submit(el => {
			el.preventDefault()
			addQuestion(el)
			callOnMod();
		});

		function addQuestion(el) {
			let url = `{{ url('add-question') }}`;
			let data = new FormData(el.target)

			goPost(url, data)
				.then(res => {
					if (res.status === 200 || res.status === 201) {
						$("#question-alert").html(res.message);
						$("#question-alert").removeClass('d-none');
						fadeAlert();
					}
				})
				.catch(err => {
					console.error(err)
				})
		}
		// Add questions

		// View questions
		function displayQuestion() {
			let getUrl = "{{url('get-question')}}";

			$.get(getUrl, function (data, status) {
				$("#question-view").empty();
				$("#question-view").html(data);
				console.log(res)
			});
		}
		// View question categories

		// Edit Questions
		function editQuestion(el, questionId) { // Function to Populate and Pop Edit Modal (GET)
			let getUrl = "{{url('get-question')}}";
			getUrl = getUrl + "/" + questionId;

			$.get(getUrl, function (data, status) {
				$("#quest-edit-view").empty();
				$("#quest-edit-view").html(data);
				$("#question-modal").modal("show");
				console.log(data)
			});
		}

		function updateQuestion(el) { // Function to update question (POST)
			let url = `{{ url('update-question') }}`;
			let data = new FormData(el.target)

			goPost(url, data)
				.then(res => {
					if (res.status === 200 || res.status === 201) {
						$("#question-alert").html(res.message);
						$("#question-alert").removeClass('d-none');
						fadeAlert();
					}
				})
				.catch(err => {
					console.error(err)
				})
		}
		// Edit Questions

		// Delete Question
		function deleteQuestion(el, questionId) { // Function to Populate and Pop Edit Modal (GET)
			let getUrl = "{{url('delete-question')}}";
			getUrl = getUrl + "/" + questionId;

			$.get(getUrl, function (data, status) {
				callOnMod()
				$("#question-alert").html(data.message);
				$("#question-alert").removeClass('d-none');
				fadeAlert();
			});
		}
		// Delete Question

		// Generic Ajax POST function
		function goPost(url, data) {
			return new Promise((resolve, reject) => {
				$.ajax({
					type: "POST",
					url,
					data,
					processData: false,
					contentType: false,
					statusCode: {
						200: (res) => {
							res.status = 200;
							resolve(res);
						},
						201: (res) => {
							res.status = 200;
							resolve(res);
						},
						500: (err) => {
							err.status = 500;
							reject(err);
						},
						404: (err) => {
							err.status = 404;
							reject(err);
						},
						419: (err) => {
							err.status = 419;
							reject(err);
						},
					},
				});
			});
		}

		// Function To Be Called On (C.R.U.D)
		function callOnMod() {
			displayCategory();
			displayQuestions();
			categorySelect();
		}

		// Function To Fade Alert
		function fadeAlert() {
			setTimeout(function () {
				$(".alert").empty();
				$(".alert").addClass('d-none');
			}, 3000);
		}
	</script>
	<!-- Necessary-JavaScript-File -->
	<!-- //js -->


</body>

</html>