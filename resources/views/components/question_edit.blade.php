<div class="modal fade" id="question-modal" tabindex="-1" data-backdrop="static" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" id="edit-question">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="question_id" value="{{$question->question_id}}">
                    <input type="hidden" value="{{$question->options_id}}" name="option_id">
                    <div>
                        <label for="category">Question</label>
                        <!-- <input type="text" name="name" value="{{$question->question}}" required> -->
                        <textarea name="name" style="width: 100%;" rows="10"
                            required>{{trim($question->question)}}</textarea>
                    </div>
                    <div>
                        <h6 for="category"><strong>Options:</strong></h6>
                        @php $alpha = 'A'; @endphp
                        @foreach($question->options as $option)
                        <div class="mb-1">
                            <label for="category">{{$alpha}}</label>
                            <input type="text" name="options[]" value="{{$option}}" required>
                        </div>
                        @php $alpha++; @endphp
                        @endforeach
                    </div>
                    <div class="mb-3">
                        <label for="category">Category:</label>
                        <div id="category-select">
                            <select name="category" id="" required>
                                @foreach($categories as $category)
                                @if($question->category_id == $category->id)
                                <option value="{{$category->id}}" selected>
                                    {{$category->name}}
                                </option>
                                @else
                                <option value="{{$category->id}}">
                                    {{$category->name}}
                                </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn">Save changes</button>
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    // Update questions
    $('#edit-question').submit(el => {
        el.preventDefault();
        updateQuestion(el);
        callOnMod();
    });
</script>