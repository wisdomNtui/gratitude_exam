<div class="modal fade" id="category-modal" tabindex="-1" data-backdrop="static" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" id="edit-category">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="colmd-6">
                        @csrf
                        <div>
                            <label for="category">Category</label>
                            <input type="text" name="name" value="{{$category->name}}" required>
                            <input type="hidden" name="category_id" value="{{$category->id}}">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn">Save changes</button>
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    // Update question categories
    $('#edit-category').submit(el => {
        el.preventDefault();
        updateCategory(el);
        callOnMod();
    });
</script>