<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Unprotected Routes
Route::prefix('/auth')->group(function () {
    Route::post('register', 'API\AuthController@register');
    Route::post('login', 'API\AuthController@login')->name('login');
});
// Protected Routes
Route::prefix('/')->middleware('auth:api')->group(function () {
    // Homepage Route
    Route::get('categories', 'API\ExamController@index');

    // Route To Add Question Category
    Route::post('add-category', 'API\ExamController@add_category');

    // Route To Get Question Category List
    Route::get('get-category/{category_id?}', 'API\ExamController@get_category');

    // Route To Update Question Category
    Route::post('update-category', 'API\ExamController@update_category');

    // Route To Delete Question Category
    Route::get('delete-category/{category_id}', 'API\ExamController@delete_category');

    // Route To Get Question Category
    Route::get('get-category/{category_id?}', 'API\ExamController@get_category');

    // Route To Add Question
    Route::post('add-question', 'API\ExamController@add_question');

    // Route To Get Question List
    Route::get('get-question/{question_id?}', 'API\ExamController@get_question');

    // Route To Update Question
    Route::post('update-question', 'API\ExamController@update_question');

    // Route To Delete Question
    Route::get('delete-question/{question_id}', 'API\ExamController@delete_question');

    // Route To Populate the Category Select Input
    Route::get('category-select', 'API\ExamController@category_select');

    // Route To Logout User
    Route::post('logout', 'API\AuthController@logout');
});
